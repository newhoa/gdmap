/* Copyright (C) 2005-2008 sgop@users.sourceforge.net
 * This is free software distributed under the terms of the GNU Public
 * License.  See the file COPYING for details.
 */
/* $Revision$
 * $Date$
 * $Author$
 */

#ifndef _GUI_ABOUT_H_
#define _GUI_ABOUT_H_

void gui_show_about(GtkWindow* parent);

#endif
